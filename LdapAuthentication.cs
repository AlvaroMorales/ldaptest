﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Data;
using System.Data.SqlClient;

namespace Datos
{
    public class LdapAuthentication
    {
        private String _path;
        private String _filterAttribute;
        SqlConnection cnx;
        Conexion miConex = new Conexion();
        SqlCommand cmd = new SqlCommand();

        public class Result
        {
            public bool Success { get; set; }
            public string mensaje { get; set; }
            public string ErrorMessage { get; set; }
        }
        public LdapAuthentication(String path)
        {
            _path = path;
        }
        public LdapAuthentication()
        {
            cnx = new SqlConnection(miConex.getConexion());
        }
        public bool IsAuthenticated(String domain, String username, String pwd)
        {
            String domainAndUsername = domain + @"\" + username;
            DirectoryEntry entry = new DirectoryEntry(_path, domainAndUsername, pwd);
            bool isValid = false;
            try
            {	//Bind to the native AdsObject to force authentication.			
                Object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + username + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (null == result)
                {
                    isValid = false;
                }

                //Update the new path to the user in the directory.
                _path = result.Path;
                _filterAttribute = (String)result.Properties["cn"][0];
                isValid = true;

            }
            catch (Exception ex)
            {
                isValid = false;
                // throw new Exception("Error autenticando usuario. " + ex.Message);
            }

            return isValid;
        }



        public string GetFullName(string userName)
        {
            string fullName = null;
            string domain = "acáVaElNombreDelDominio.local";

            DirectoryEntry entry = new DirectoryEntry("LDAP://" + domain);
            using (DirectorySearcher searcher = new DirectorySearcher(entry))
            {
                searcher.Filter = string.Format("(&(objectClass=user)(objectCategory=person)(samAccountName={0}))", userName);
                SearchResult result = searcher.FindOne();

                if (null != result)
                {
                    fullName = result.GetDirectoryEntry().Properties["displayName"].Value.ToString();
                }
            }

            return fullName;
        }

        public string getEmail(string userName)
        {
            string emailAddres = null;
            string domain = "acáVaElNombreDelDominio.local";

            DirectoryEntry entry = new DirectoryEntry("LDAP://" + domain);
            using (DirectorySearcher searcher = new DirectorySearcher(entry))
            {
                searcher.Filter = string.Format("(&(objectClass=user)(objectCategory=person)(samAccountName={0}))", userName);
                SearchResult result = searcher.FindOne();

                if (null != result)
                {
                    //   emailAddres = result.GetDirectoryEntry().Properties["mail"].Value.ToString();
                }
            }

            return emailAddres;
        }

        public List<String> getDistinctProperty(String propertyName)
        {
            List<String> auxArray = new List<String>();
            //List<String> usuarios = new List<String>();
            using (var context = new PrincipalContext(ContextType.Domain))
            {
                using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                {
                    foreach (var result in searcher.FindAll())
                    {
                        DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                        int flag = (int)de.Properties["userAccountControl"].Value;
                        if (de.Properties[propertyName].Value != null &&
                         !Convert.ToBoolean(flag & 0x0002))
                        {
                            auxArray.Add(de.Properties[propertyName].Value.ToString());
                            /* usuarios.Add(
                                 de.Properties["department"].Value.ToString() + "|" +
                                 de.Properties["SAMAccountName"].Value.ToString() + "|"+
                                 de.Properties["Name"].Value.ToString());*/
                        }
                    }
                }
            }
            String[] auxArrayDistinct = auxArray.ToArray().Distinct().ToArray();
            List<String> distinctValues = new List<String>();

            for (int i = 0; i <= auxArrayDistinct.Length - 1; i++)
            {
                distinctValues.Add(auxArrayDistinct[i]);
            }
            return distinctValues;
        }


        public string getOffice(string userName)
        {
            string office = null;
            string domain = "acáVaElNombreDelDominio.local";

            DirectoryEntry entry = new DirectoryEntry("LDAP://" + domain);
            using (DirectorySearcher searcher = new DirectorySearcher(entry))
            {
                searcher.Filter = string.Format("(&(objectClass=user)(objectCategory=person)(samAccountName={0}))", userName);
                SearchResult result = searcher.FindOne();

                if (null != result)
                {
                    office = result.GetDirectoryEntry().Properties["mail"].Value.ToString();
                }
            }

            return office;
        }

        public List<GroupPrincipal> getGroupsForUser(String userName)
        {
            List<GroupPrincipal> resultado = new List<GroupPrincipal>();
            PrincipalContext dominio = new PrincipalContext(ContextType.Domain);
            UserPrincipal user = UserPrincipal.FindByIdentity(dominio, userName);
            if (user != null)
            {
                PrincipalSearchResult<Principal> groups = user.GetAuthorizationGroups();
                foreach (Principal p in groups)
                {
                    if (p is GroupPrincipal)
                    {
                        resultado.Add((GroupPrincipal)p);
                    }
                }
            }
            return resultado;
        }
 


    }
}